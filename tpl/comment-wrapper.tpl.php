<?php

/**
 * @file comment-wrapper.tpl.php
 * Default theme implementation to wrap comments.
 */
?>
<div id="comments">
  <h3><?php print t('Comments'); ?></h3>
  <?php print $content; ?>
</div>
